import Card

class Hand:
    def __init__(self):
        self.value = 0 #value of the hand
        self.soft = False #if the hand contains an ace as an 11
    
    #discards any cards presently held, restoring the state
    #of the hand to that of an empty blackjack hand.
    def discard_all(self):
        self.value = 0
        self.soft = False

    #adds the card "c" to those presently held.
    def add_card(self, c):
        if c.rank == 12: #if ace
            if self.soft == True:
                self.value += 1
            else: #self.soft == False
                self.value += 11
                self.soft = True
        else:
            self.value += {
                0 : 2,
                1 : 3,
                2 : 4,
                3 : 5,
                4 : 6,
                5 : 7,
                6 : 8,
                7 : 9,
                8 : 10,
                9 : 10,
                10 : 10,
                11 : 10
            }[c.rank]

        if self.value > 21 and self.soft == True:
            self.value -= 10
            self.soft = False
            
if __name__ == "__main__":
    c = Card.Card()
    h = Hand()
    assert (h.value == 0 and h.soft == False)
    h.add_card(c)
    assert (h.value == 2 and h.soft == False)
    h.discard_all()
    assert (h.value == 0 and h.soft == False)

    ace = Card.Card('ace', 'spades')
    king = Card.Card('king', 'spades')
    h.add_card(ace)
    assert (h.value == 11 and h.soft == True)
   
    h.add_card(ace)
    assert (h.value == 12 and h.soft == True)

    h.add_card(king)
    assert (h.value == 12 and h.soft == False)
    
    h.add_card(c)
    assert (h.value == 14 and h.soft == False)

    print "\nyou passed!"
