#this module contains the Card class

suit_names = ["Spades", "Hearts", "Clubs", "Diamonds"]
rank_names = ["Two", "Three", "Four", "Five", "Six", 
              "Seven", "Eight", "Nine", "Ten", "Jack", 
              "Queen", "King", "Ace"] 

suit_dic = {"Spades":0, "Hearts":1, "Clubs":2, "Diamonds":3}
rank_dic = {"Two":0, "Three":1, "Four":2, "Five":3, "Six":4, 
              "Seven":5, "Eight":6, "Nine":7, "Ten":8, "Jack":9, 
              "Queen":10, "King":11, "Ace":12}

class Card:

    #initializes card to two of clubs or specified card
    def __init__(self, rank_in = 0, suit_in = 0):
        #check if they gave us the names rather than numerical values
        if isinstance(rank_in, basestring):
            rank_in = rank_in.title()
            rank_in = rank_dic[rank_in]

        if isinstance(suit_in, basestring): 
            suit_in = suit_in.title()
            suit_in = suit_dic[suit_in]

        self.rank = rank_in
        self.suit = suit_in

    #overloading the pring operator
    def __str__(self):
        return "%s of %s" % (rank_names[self.rank], suit_names[self.suit])

if __name__ == "__main__":
    herp = Card('three', 'spades')
    derp = Card()
    print derp
    print herp
    herp = Card(1, 0)
    derp = Card(5, 2)
    print herp
    print derp
