//David Snider with maia hoberman
#include "Card.h"
#include <cassert>
#include <iostream>
#include <cstring>
using namespace std;

static const char *SUIT_NAMES[] = {"Spades", "Hearts", "Clubs", "Diamonds"};
static const int SUIT_SIZE = 4;
static const char *RANK_NAMES[] = {"Two", "Three", "Four", "Five", "Six", 
				   "Seven", "Eight", "Nine", "Ten", "Jack", 
				   "Queen", "King", "Ace"};
static const int RANK_SIZE = 13;

//EFFECTS: initializes Card to Two of Spades
Card :: Card(){
  rank = static_cast<Rank>(0);
  suit = static_cast<Suit>(0);
}

//EFFECTS: initializes Card with rank_in and suit_in
Card :: Card(Rank rank_in, Suit suit_in){
  rank = static_cast<Rank>(rank_in);
  suit = static_cast<Suit>(suit_in);
}  

//EFFECTS: returns rank
Card :: Rank Card :: get_rank() const{
  return(rank);
}

//EFFECTS: returns suit
Card :: Suit Card :: get_suit() const{
  return(suit);
}

//EFFECTS: Writes the Card to the stream
//  for example "Two of Spades"
std::ostream& operator<< (std::ostream& os, const Card& c){
  os << RANK_NAMES[c.get_rank()] << " of " << SUIT_NAMES[c.get_suit()];
  return os;
}
