
#include "Card.h"
#include "Hand.h"
#include "Player.h"
#include <cassert>
#include <iostream>
#include <cstring>
using namespace std;


/*
 public:
   
 int bet(unsigned int bankroll, unsigned int minimum); 
 // REQUIRES: bankroll >= minimum
 // EFFECTS: returns the player's bet, between minimum and bankroll 
 // inclusive
 
 bool draw(
    Card dealer, // Dealer's "up card"
    const Hand &player // Player's current hand
 )
 // EFFECTS: returns true if the player wishes to be dealt another
 // card, false otherwise.
 
 void expose(Card c);
 
 void shuffled();
 // EFFECTS: tells the player that the deck has been re-shuffled.
 */
int main() {

  //things to test:
  //bet, draw, expose, suffled, player factory
  //competitor


  Player* simple = player_factory("simple");
  Player* competitor = player_factory("competitor");
  Player* counting = player_factory("counting");
  Hand hand;

  assert (simple->bet(15, 4) == 4); 
  assert (simple->bet(2, 0) == 0); 
  assert (competitor->bet(15, 4) == 4);
  assert (counting->bet(15, 4) == 4); 

  Card two(Card::TWO, Card::CLUBS);  
  Card three(Card::THREE, Card::CLUBS);  
  Card four(Card::FOUR, Card::CLUBS);  
  Card five(Card::FIVE, Card::CLUBS);
  Card six(Card::SIX, Card::CLUBS);  
  Card seven(Card::SEVEN, Card::CLUBS);  
  Card eight(Card::EIGHT, Card::CLUBS);  
  Card nine(Card::NINE, Card::CLUBS);  
  Card ten(Card::TEN, Card::CLUBS);
  Card ace(Card::ACE, Card::CLUBS);  


  competitor->expose(five); //count = 2
  assert (competitor->bet(15,4) == 8);
  hand.add_card(five); //hand is 5
  hand.add_card(ace); //hand is 16
  assert (hand.hand_is_soft());
  hand.add_card(ten); //hand is 16
  assert (!hand.hand_is_soft());
  //make sure all the player stuff works
  hand.discard_all(); //hand is 0
  
  //!!!!
  //make sure simple always bets ming

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);

  hand.add_card(five); //hnd is 5
  assert(simple->draw( ace ,hand ));
  hand.add_card(six); //hand is at 11
  assert(simple->draw( ace ,hand ));


  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);


  hand.discard_all();
  hand.add_card(five); //hand at 5
  hand.add_card(five); //hand at 10
  hand.add_card(two); //hand at 12
  assert(simple->draw( ace ,hand ));
  assert(simple->draw( two ,hand ));
  assert(!simple->draw( six ,hand ));
  assert(!simple->draw( four ,hand ));
  assert(!simple->draw( five ,hand ));

  hand.add_card(ace); //hand at 13
  assert(!simple->draw( two ,hand ));
  assert(!simple->draw( three ,hand ));
  assert(!simple->draw( four ,hand ));
  assert(!simple->draw( five ,hand ));
  assert(!simple->draw( six ,hand ));
  assert(simple->draw( seven ,hand ));

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);



  hand.add_card(ace); //hand at 14
  assert(!simple->draw( two ,hand ));
  assert(!simple->draw( three ,hand ));
  assert(!simple->draw( four ,hand ));
  assert(!simple->draw( five ,hand ));
  assert(!simple->draw( six ,hand ));
  assert(simple->draw( seven ,hand ));

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);


  hand.add_card(ace); //hand at 15
  assert(!simple->draw( two ,hand ));
  assert(!simple->draw( three ,hand ));
  assert(!simple->draw( four ,hand ));
  assert(!simple->draw( five ,hand ));
  assert(!simple->draw( six ,hand ));
  assert(simple->draw( seven ,hand ));

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);



  hand.add_card(ace); //hand at 16
  assert(!simple->draw( two ,hand ));
  assert(!simple->draw( three ,hand ));
  assert(!simple->draw( four ,hand ));
  assert(!simple->draw( five ,hand ));
  assert(!simple->draw( six ,hand ));
  assert(simple->draw( seven ,hand ));

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);

  hand.add_card(ace); //hand at 17
  assert(!simple->draw(two, hand));

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);



  
  hand.add_card(ace); //hand at 18
  assert(!simple->draw(two, hand));


  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);

  
  hand.add_card(ace); //hand at 19
  assert(!simple->draw(two, hand));


  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);


  hand.add_card(ace); //hand at 20
  assert(!simple->draw(two, hand));

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);

  
  hand.add_card(ace); //hand at 21
  assert(!simple->draw(two, hand));
  
  //put this back in for testing later
  // hand.add_card(ace); //hand at 22 //this should fail
  // assert(!simple->draw(two, hand));
  
  hand.discard_all(); //hand at 0
  hand.add_card(ace); //hand at soft 11;
  assert(simple->draw(two, hand));
  hand.add_card(six); //hand at soft 17
  assert(simple->draw(two, hand));
  hand.add_card(ace); //hand at soft 18
  assert (hand.hand_is_soft());

  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);


  assert(!simple->draw(two, hand));
  assert(!simple->draw(seven, hand));
  assert(!simple->draw(eight, hand));
  assert(simple->draw(three, hand));
  assert(simple->draw(nine, hand));
  
  hand.add_card(ace); //hand at soft 19
  assert(!simple->draw(eight, hand));
  assert(!simple->draw(five, hand));
  assert(!simple->draw(ten, hand));
  assert(!simple->draw(two, hand));


  assert(simple->bet(20,5) == 5);
  assert(simple->bet(20,0) == 0);
  assert(simple->bet(20,20) == 20);


  //##########################################################
  //tests for counting

  counting->shuffled(); //count at 0
  hand.discard_all();
  hand.add_card(five); //hand is 5
  counting->expose(five); //count at 1
  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  assert(counting->draw( ace ,hand ));
  hand.add_card(six); //hand is at 11
  counting->expose(six); //count at 2
  assert(counting->draw( ace ,hand ));
  assert(counting->bet(20, 5) == 10);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 20);
  assert(counting->bet(20, 11) == 20);

  hand.discard_all();
  hand.add_card(five); //hand at 5
  counting->expose(five); //count at 3
  assert(counting->bet(20, 5) == 10);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 20);
  assert(counting->bet(20, 11) == 20);


  counting->shuffled(); //count at 0

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);


  hand.add_card(five); //hand at 10
  counting->expose(five); //count at 1
  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  hand.add_card(two); //hand at 12
  counting->expose(two); //count at 2

  assert(counting->bet(20, 5) == 10);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 20);
  assert(counting->bet(20, 11) == 20);

  assert(counting->draw( ace ,hand ));
  assert(counting->draw( two ,hand ));
  assert(!counting->draw( six ,hand ));
  assert(!counting->draw( four ,hand ));
  assert(!counting->draw( five ,hand ));

  hand.add_card(ace); //hand at 13
  counting->expose(ace); //count at 1

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);


  assert(!counting->draw( two ,hand ));
  assert(!counting->draw( three ,hand ));
  assert(!counting->draw( four ,hand ));
  assert(!counting->draw( five ,hand ));
  assert(!counting->draw( six ,hand ));
  assert(counting->draw( seven ,hand ));

  hand.add_card(ace); //hand at 14
  counting->expose(ace); //count at 0

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  assert(!counting->draw( two ,hand ));
  assert(!counting->draw( three ,hand ));
  assert(!counting->draw( four ,hand ));
  assert(!counting->draw( five ,hand ));
  assert(!counting->draw( six ,hand ));
  assert(counting->draw( seven ,hand ));

  hand.add_card(ace); //hand at 15
  counting->expose(ace); //count at -1

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  assert(!counting->draw( two ,hand ));
  assert(!counting->draw( three ,hand ));
  assert(!counting->draw( four ,hand ));
  assert(!counting->draw( five ,hand ));
  assert(!counting->draw( six ,hand ));
  assert(counting->draw( seven ,hand ));

  hand.add_card(ace); //hand at 16
  counting->expose(ace); //count at -2

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  assert(!counting->draw( two ,hand ));
  assert(!counting->draw( three ,hand ));
  assert(!counting->draw( four ,hand ));
  assert(!counting->draw( five ,hand ));
  assert(!counting->draw( six ,hand ));
  assert(counting->draw( seven ,hand ));

  hand.add_card(ace); //hand at 17
  counting->expose(ace); //count at -3
  assert(!counting->draw(two, hand));
  
  hand.add_card(ace); //hand at 18
  counting->expose(ace); //count at -4
  assert(!counting->draw(two, hand));
  
  hand.add_card(ace); //hand at 19
  counting->expose(ace); //count at -5
  assert(!counting->draw(two, hand));

  hand.add_card(ace); //hand at 20
  counting->expose(ace); //count at -6
  assert(!counting->draw(two, hand));
  
  hand.add_card(ace); //hand at 21
  counting->expose(ace); //count at -7
  assert(!counting->draw(two, hand));
  
  //put this back in for testing later
  // hand.add_card(ace); //hand at 22 //this should fail
  // assert(!counting->draw(two, hand));
  
  counting->shuffled(); //count at 0

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  hand.discard_all(); //hand at 0
  hand.add_card(ace); //hand at soft 11;
  counting->expose(ace); //count at -1 

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);


  assert(counting->draw(two, hand));
  hand.add_card(six); //hand at soft 17
  counting->expose(six);//count at 0

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);


  assert(counting->draw(two, hand));
  hand.add_card(ace); //hand at soft 18
  counting->expose(ace);//count at -1

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  assert (hand.hand_is_soft());

  assert(!counting->draw(two, hand));
  assert(!counting->draw(seven, hand));
  assert(!counting->draw(eight, hand));
  assert(counting->draw(three, hand));
  assert(counting->draw(nine, hand));
  
  hand.add_card(ace); //hand at soft 19
  counting->expose(ace); //count at -2

  assert(counting->bet(20, 5) == 5);
  assert(counting->bet(20, 0) == 0);
  assert(counting->bet(20, 10) == 10);
  assert(counting->bet(20, 11) == 11);

  assert(!counting->draw(eight, hand));
  assert(!counting->draw(five, hand));
  assert(!counting->draw(ten, hand));
  assert(!counting->draw(two, hand));

  //decently good tests for counting
  //test other later




  cout << "YOU PASSED!" << endl;
  return 0;
}
