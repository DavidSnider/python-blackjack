//David Snider with maia hoberman
#include "Card.h"
#include "Deck.h"
#include "Hand.h"
#include <cassert>
#include <iostream>
#include <cstring>
using namespace std;

// EFFECTS: establishes an empty blackjack hand.
Hand :: Hand(){
  value = 0;
  soft = false;
}

// MODIFIES: this
// EFFECTS: discards any cards presently held, restoring the state
// of the hand to that of an empty blackjack hand.
void Hand :: discard_all(){
  value = 0;
  soft = false;
}

// MODIFIES: this
// EFFECTS: adds the card "c" to those presently held.
void Hand :: add_card(Card c){
  switch (c.get_rank()){
  case 0 ://2 //change to Card::TWO
  case 1 ://3
  case 2 ://4
  case 3 ://5
  case 4 ://6
  case 5 ://7
  case 6 ://8
  case 7 ://9
    value += c.get_rank() + 2;    
    if (soft){
      if(value > 21){
	value -= 10;
	soft = false;
      }
    }
    break;
  case 8 ://10
  case 9 ://jack
  case 10 ://queen
  case 11 ://king
    value += 10;
    if (soft){
      if(value > 21){
	value -= 10;
	soft = false;
      }
    }
    break;
  case 12 :
    if (!soft && value < 11){ //11 would work but adding an 11 would bust
      //hand with no aces ^
      value += 11; // fix this
      soft = true;
    }
    else if (!soft){
      //hand with an aces only as 1s
      value += 1;
    }
    else{//hand with ace as an 11
      value += 1;
      if(value > 21){
	value -= 10;
	soft = false;
      }
      assert(value <= 21);
    }
    break;
  }
}
// EFFECTS: returns the present value of the blackjack hand, the
// highest blackjack total possible without going over 21.  If the hand is
// over 21, any value over 21 may be returned.
int Hand :: hand_value() const{
  return (value);
}

// EFFECTS: return true if and only if at least one ACE is present, and 
// its value is counted as 11 rather than 1.
bool Hand :: hand_is_soft() const{
  return(soft);
}
