//David Snider with maia hoberman
#include "Card.h"
#include "Deck.h"
#include <cassert>
#include <iostream>
#include <cstring>
using namespace std;

// EFFECTS: constructs a "newly opened" deck of cards.  first the
// spades from 2-A, then the hearts, then the clubs, then the
// diamonds.  The first card dealt should be the 2 of Spades.
Deck :: Deck(){
  reset();
}
// MODIFIES: this
// EFFECTS: resets the deck to the state of a "newly opened" deck
// of cards:
void Deck :: reset(){
  for (int j = 0; j < 4; j++){
    for (int i = 0; i < 13; i++){
      cards[13*j+i] = Card( static_cast<Card :: Rank>(i) , 
			    static_cast<Card :: Suit>(j) );
    }
  }
  next = cards;
}
// REQUIRES: n is between 0 and 52, inclusive.
// MODIFIES: this
// EFFECTS: cut the deck into two segments: the first n cards,
// called the "left", and the rest called the "right".  Note that
// either right or left might be empty.  Then, rearrange the deck
// to be the first card of the right, then the first card of the
// left, the 2nd of right, the 2nd of left, and so on.  Once one
// side is exhausted, fill in the remainder of the deck with the
// cards remaining in the othe rside.  Finally, make the first
// card in this shuffled deck the next card to deal.  For example,
// shuffle(26) on a newly-reset() deck results in: 2-clubs,
// 2-spades, 3-clubs, 3-spades ... A-diamonds, A-hearts.
void Deck :: shuffle(int n){
  int leftsize = n, rightsize = 52 - n, rightnum = 0, leftnum = 0;
  Deck left, right;
  next = cards;
  for (int i = 0; i < 52 ; i++){
    if (i < n){
      left.cards[i] = cards[i];
    }
    else{
      right.cards[i - n] = cards[i];
    }     
  }
  for (int i = 0; i < 52; ){
    if (rightsize > 0){
      cards[i] = right.cards[rightnum];
      i++; rightnum++; rightsize--;
    }
    if (leftsize > 0){
      cards[i] = left.cards[leftnum];	
      i++; leftnum++; leftsize--;
    }
  }
  next = cards;
}

// REQUIRES: deck is not empty
// MODIFIES: this
// EFFECTS: returns the next card to be dealt.
Card Deck :: deal(){
  assert ((next-1)->get_rank() != cards[51].get_rank() ||
	  (next-1)->get_suit() != cards[51].get_suit() );
   
   return *(next++);
}

// EFFECTS: returns the number of cards in the deck that have not
// been dealt since the last reset/shuffle.
int Deck :: cards_remaining() const{
  if (next == cards) return 52;

  for ( int i = 0; i < 52; i++){
    if ((next-1)->get_rank() == cards[i].get_rank() &&
	(next-1)->get_suit() == cards[i].get_suit() )
      return (51 - i);
  }
  assert(0);
}
