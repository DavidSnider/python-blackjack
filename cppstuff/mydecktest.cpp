// This is an example test case which runs a simple test involving
// your shuffle and deal functions.

#include "Deck.h"
#include "Card.h"
#include <cassert>
#include <iostream>
using namespace std;


int main()
{
  Deck d;
  
  Card before = d.deal();
  d.shuffle(26); // Should replace the dealt card before shuffling
  
  d.deal(); // Throw one away
  Card after = d.deal(); // Should be the same as before.
  
  // cout << "First card in new deck: " << before << endl;
  // cout << "Second card in shuffled deck: " << after << endl;
  assert(before.get_rank() == after.get_rank());
  assert(before.get_suit() == after.get_suit());
  
  Deck d2;
  //check default constructor workingness######
  for (int i = 0; i < 52; i++){
    Card herp = d2.deal();  
    assert (herp.get_rank() == i % 13);
    assert (herp.get_suit() == i/13);
  }
  
  //check reset workingness##################
  d2.reset();
  for (int i = 0; i < 52; i++){
    Card herp = d2.deal();
    // cout << herp << endl;
    // cout << d2.cards_remaining() << endl;
    assert (d2.cards_remaining()  == 51-i);
    // check cards remaining #############
    assert (herp.get_rank() == i % 13);
    assert (herp.get_suit() == i/13);
  }
  
  d2.reset();
  /*
  d2.shuffle(36);
  for (int i = 0; i < 52 ; i++){
    Card herp = d2.deal();
    cout << herp << endl;
    } 
  d2.reset();
  */
  d2.shuffle(26);
  for (int i = 0 ; i < 26; i++){
    Card herp = d2.deal();
    Card derp = d2.deal();
    assert (herp.get_rank() == derp.get_rank());
    assert (herp.get_suit() == derp.get_suit() + 2);
  }
  d2.reset();

  d2.shuffle(52);
  for (int i = 0 ; i < 52; i++){
    Card herp = d2.deal();  
    assert (herp.get_suit() == i / 13 );
    assert (herp.get_rank() == i % 13);
  }
  d2.reset();

  d2.shuffle(0);
  for (int i = 0 ; i < 52; i++){
    Card herp = d2.deal();  
    assert (herp.get_suit() == i / 13 );
    assert (herp.get_rank() == i % 13);
  }
  d2.reset();  

  //check cards_remaining ######
  for (int i = 0; i < 52; i++){
    d2.deal();
    assert (d2.cards_remaining()  == 51-i);
  }


  cout << "YOU PASSED!" << endl;
  return 0;
}
