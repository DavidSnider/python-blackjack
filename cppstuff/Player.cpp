//David Snider with maia hoberman
#include "Card.h"
#include "Hand.h"
#include "Player.h"
#include <cassert>
#include <iostream>
#include <cstring>
using namespace std;

class Simple : public Player{
  // a virtual derived class from player that plays avery simple 
  // game of blackjack as described in the spec
public:

  // REQUIRES: bankroll >= minimum
  // EFFECTS: returns the player's bet, between minimum and bankroll 
  // inclusive
  virtual int bet(unsigned int bankroll, unsigned int minimum){
    return minimum;
  }
  
  
  // EFFECTS: returns true if the player wishes to be dealt another
  // card, false otherwise.
  virtual bool draw(
		    Card dealer, // Dealer's "up card"
		    const Hand &player // Player's current hand
		    ){
    
    Card :: Rank dcard;
    if (dealer.get_rank() < Card::TEN ){ dcard = dealer.get_rank(); }
    else if (dealer.get_rank() < Card::ACE ) { dcard = Card::TEN; }
    else dcard = Card::ACE;
    
    int pval = player.hand_value();
    assert (pval > 0 && pval < 22);
   
    if (!player.hand_is_soft()){
      if (pval <= 11 )
	return true;
      if (pval == 12){
	if(dcard > Card::THREE && dcard < Card::SEVEN)
	  return false;
	return true;
      }
      if (pval > 12 && pval < 17){
	if(dcard >= Card::TWO && dcard < Card::SEVEN)
	  return false;
	return true;
      }
      if (pval > 16 )
	return false;
    }
    else{ //player has soft hand
      if (pval < 18)
	return true;
      if (pval > 18)
	return false;
      if (dcard == Card::TWO || dcard == Card::SEVEN || dcard == Card::EIGHT)
	return false;
      else 
	return true;
    }
    assert(false&&"reached end of draw without returning");
  }

   
  // EFFECTS: allows the player to "see" the newly-exposed card c.
  // For example, each card that is dealt "face up" is expose()d.
  // Likewise, if the dealer must show his "hole card", it is also
  // expose()d.  Note: not all cards dealt are expose()d---if the
  // player goes over 21 or is dealt a natural 21, the dealer need
  // not expose his hole card.
  virtual void expose(Card c){
    return;
  }

  // EFFECTS: tells the player that the deck has been re-shuffled.
  virtual void shuffled(){
    return;
  }
};

// a virtual derived class from the simple player that plays a
// a slightly more complex game of black, including betting variations
// due to the count of the deck. Implementation based off spec
class Counting : public Simple{
  int count; //running sum for counting cards
public:
  // REQUIRES: bankroll >= minimum
  // EFFECTS: returns the player's bet, between minimum and bankroll 
  // inclusive
  Counting(){
    count = 0;
  }

  virtual int bet(unsigned int bankroll, unsigned int minimum){
    if (count > 1){
      if (bankroll >= 2*minimum) return 2*minimum;
      else return bankroll;
    }
    return minimum;
  }
   
  // EFFECTS: allows the player to "see" the newly-exposed card c.
  // For example, each card that is dealt "face up" is expose()d.
  // Likewise, if the dealer must show his "hole card", it is also
  // expose()d.  Note: not all cards dealt are expose()d---if the
  // player goes over 21 or is dealt a natural 21, the dealer need
  // not expose his hole card.
  virtual void expose(Card c){
    if (c.get_rank() < Card::SEVEN )
      count++;
    else if (c.get_rank() > Card::NINE)
      count--;
  }

  // EFFECTS: tells the player that the deck has been re-shuffled.
  virtual void shuffled(){
    count = 0;
  }
};

class Competitor : public Simple{ //Hi-Opt II counter
  int count; //running sum for counting cards
  int numaces;
public:
  // REQUIRES: bankroll >= minimum
  // EFFECTS: returns the player's bet, between minimum and bankroll 
  // inclusive
  Competitor(){
    count = 0;
    numaces = 4;
  }

  virtual int bet(unsigned int bankroll, unsigned int minimum){
    
    if (count > 5 && numaces > 2){
      if (bankroll >= 10*minimum) return 10*minimum;
      else return bankroll;
    }
    else if (count > 5){
      if (bankroll >= 8*minimum) return 8*minimum;
      else return bankroll;
    }
    else if (count > 3 && numaces > 3){
      if (bankroll >= 8*minimum) return 8*minimum;
      else return bankroll;
    }
    else if (count > 3 && numaces > 2){
      if (bankroll >= 6*minimum) return 6*minimum;
      else return bankroll;
    }
    else if (count > 3){
      if (bankroll >= 4*minimum) return 4*minimum;
      else return bankroll;
    } 
    else if (count > 1){
      if (bankroll >= 2*minimum) return 2*minimum;
      else return bankroll;
    }
    return minimum;
  }
  
  // EFFECTS: allows the player to "see" the newly-exposed card c.
  // For example, each card that is dealt "face up" is expose()d.
  // Likewise, if the dealer must show his "hole card", it is also
  // expose()d.  Note: not all cards dealt are expose()d---if the
  // player goes over 21 or is dealt a natural 21, the dealer need
  // not expose his hole card.
  virtual void expose(Card c){
    if (c.get_rank() < Card::FOUR )
      count++;
    else if (c.get_rank() < Card::SIX)
      count += 2;
    else if (c.get_rank() < Card::EIGHT)
      count++;
    else if (c.get_rank() > Card::KING){
      numaces--;
    }
    else if (c.get_rank() > Card::NINE)
      count -= 2;
   
    //cout << "REMOVE THIS!!, count after next card is: " << count << endl;    
  }

  // EFFECTS: tells the player that the deck has been re-shuffled.
  virtual void shuffled(){
    count = 0;
    numaces = 4;
  }
};

static Simple simpleplayer;
static Counting countingplayer;
static Competitor competplayer;

// REQUIRES: s is a C-string: "simple" "counting" or "competitor"
// EFFECTS: returns a pointer to the specified Player
Player * player_factory(const char * s){
  if (strcmp(s, "simple") == 0) { return &simpleplayer;}
  if (strcmp(s, "counting") == 0) { return &countingplayer;}
  if (strcmp(s, "competitor") == 0) { return &competplayer;}
  assert(0);
}
