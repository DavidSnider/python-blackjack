#include "Card.h"
#include "Deck.h"
#include "Hand.h"
#include <cassert>
#include <iostream>
#include <cstring>
using namespace std;

int main(){
  
  Hand h;
  assert (h.hand_value() == 0);
  assert (h.hand_is_soft() == false);
  
  h.discard_all();
  assert (h.hand_value() == 0);
  assert (h.hand_is_soft() == false);

  Card c;
  
  h.add_card(c);
  assert (h.hand_value() == 2);
  assert (h.hand_is_soft() == false);
  h.add_card(c);
  assert (h.hand_value() == 4);
  assert (h.hand_is_soft() == false);

  Card c2(Card::FIVE, Card::HEARTS);
  h.add_card(c2);
  assert (h.hand_value() == 9);
  assert (h.hand_is_soft() == false);
 
  h.discard_all();
  h.add_card(c2);
  assert (h.hand_value() == 5);
  assert (h.hand_is_soft() == false);
 
  Card c3(Card::TEN, Card::CLUBS);
  Card c4(Card::ACE, Card::CLUBS);
  h.discard_all();
  h.add_card(c3);
  assert (h.hand_value() == 10);
  assert (h.hand_is_soft() == false);

  h.add_card(c4);
  assert (h.hand_value() == 21);
  assert (h.hand_is_soft() == true);

  h.add_card(c4);
  assert (h.hand_value() == 12);
  assert (h.hand_is_soft() == false);

  cout << "YOU PASSED!" << endl;
  return(0);
}
