//David Snider with maia hoberman
#include "Card.h"
#include "Hand.h"
#include "Player.h"
#include "rand.h"
#include "Deck.h"

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <cstring>

bool dealer_draw(const Hand &dealer);
using namespace std;

int main (int argc, char** argv){
  assert (argc == 4);
  int bankroll = atoi(argv[1]);
  const int hands = atoi(argv[2]);

  Player * player = player_factory(argv[3]);   
  Deck deck; 
  Hand playerHand, dealer;
  Card shownCard, holeCard, dealtCard;

  int wager = 0; int cutNum = 0;
  const int minBet = 5;
  bool keepgoing = true, busted = false;

  cout << "Shuffling the deck\n";
  for (int i = 0; i < 7; i++){
    cutNum = get_cut();
    cout << "cut at " << cutNum << endl;
    deck.shuffle(cutNum);
  }
  player->shuffled();

  if (bankroll < minBet){
    cout << "Player has " << bankroll << " after 0 hands\n";    
    return(0);
  }
  
  for (int handnum = 1; keepgoing && handnum < hands+1 ; handnum++){
    //reset all the things
    playerHand.discard_all();
    dealer.discard_all();
    busted = false;

    if (bankroll >= minBet ){ //play a hand
      cout << "Hand " << handnum << " bankroll " << bankroll << endl;
      if ( deck.cards_remaining() < 20){ //if fewer than 20 cards in deck
	cout << "Shuffling the deck\n";
	for (int i = 0; i < 7; i++){
	  cutNum = get_cut();
	  cout << "cut at " << cutNum << endl;
	  deck.shuffle(cutNum);
	}
	player->shuffled();
      }
      
      
      

      wager = player->bet(bankroll, minBet);
      //figure out player bet
      cout << "Player bets " << wager << endl;
     
      //deal the cards, one to player, one to dealer, one to player, face
      //down to dealer
      for (int i = 0; i < 2; i++){
	
	dealtCard = deck.deal();
	playerHand.add_card(dealtCard);
	player->expose(dealtCard);
	cout << "Player dealt " << dealtCard << endl;

	dealtCard = deck.deal();
	dealer.add_card(dealtCard);
	if (i == 0){
	  player->expose(dealtCard);
	  shownCard = dealtCard;
	  cout << "Dealer dealt " << dealtCard << endl;
	}
	else{
	  holeCard = dealtCard;
	}
      }
      
      //if player dealt 21, pay player 3/2 of bet, so (3*wager)/2
      //announce the win
      //goto next hand
      
      if (playerHand.hand_value() == 21){
	bankroll += (3*wager)/2;
	cout << "Player dealt natural 21\n";
	continue;
      }
      
      //play hand
      
      //player draw until stans or busts      
      while (player->draw(shownCard, playerHand)){
	dealtCard = deck.deal();
	playerHand.add_card(dealtCard);
	player->expose(dealtCard);
	cout << "Player dealt " << dealtCard << endl; 

	if (playerHand.hand_value() > 21 ){
	  busted = true;
	  break;
	}
      }
      //cout and expose() each card as dealt
      //keep a running total of what his hand is:
      
      //announce player total
      cout << "Player's total is " << playerHand.hand_value() << endl;

      //if player busts announce it
      // deduct losings from bankroll and goto next hand
      if (busted){
	cout << "Player busts\n";
	bankroll -= wager;

	//if last hand
	if (handnum == hands){
	  cout << "Player has " << bankroll << " after " << handnum
	       << " hands\n";
	}
	
     	continue;
      }
     
      //if no bust, dealer plays

      //expose and announce hole card since no bust
      player->expose(holeCard);

      cout << "Dealer's hole card is " << holeCard << endl;

      
      //announce and expose each card as dealer plays;
      //keep a running total of what his hand is:
      while (dealer_draw( dealer )){
	dealtCard = deck.deal();
	dealer.add_card( dealtCard );
	player->expose(dealtCard);
	cout << "Dealer dealt " << dealtCard << endl; 

	if (dealer.hand_value() > 21 ){
	  busted = true;
	  break;
	}
      }
     
      //announce total of dealer 
      cout << "Dealer's total is " << dealer.hand_value() << endl;
     
      //if dealer busts announce such
      //add the wager to the bankroll
      //goto next hand
      if (busted){
	cout << "Dealer busts\n";
	bankroll += wager;

	//if last hand
	if (handnum == hands){
	  cout << "Player has " << bankroll << " after " << handnum
	       << " hands\n";
	}
     	continue;
      }
      
      //if player loses
      if (dealer.hand_value() >  playerHand.hand_value() ){
	cout << "Dealer wins\n";
	bankroll -= wager;
      }//if player loses
      else if (dealer.hand_value() <  playerHand.hand_value() ){
	cout << "Player wins\n";
	bankroll += wager;
      }// if player wins
      else
	cout << "Push\n";
      //if push
	  
      // don't reset count
    }
    else{
      cout << "Player has " << bankroll << " after " << handnum
	   << " hands\n";
      keepgoing = false; 
    }
    if (handnum == hands){
      cout << "Player has " << bankroll << " after " << handnum
	   << " hands\n";
    }
  }
  return 0;
} 


//EFFECTS:  determines if the dealer should ask for another card
bool dealer_draw(const Hand &dealer){
  if (dealer.hand_value() < 17)
    return true;
  return false;
}

