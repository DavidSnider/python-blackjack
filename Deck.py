import Card

#a class containing the playing deck and the index of the next card to be played
class Deck:

    #constructs an "unopened pack." First the spades from 2-A, then the hearts, 
    #     then the clubs, then the diamonds.  
    #     The first card dealt should be the 2 of Spades.
    def __init__(self):
        self.cards = [None]*52
        for i in range(0,4):
            for j in range(0,13):
                 self.cards[j+13*i] = Card.Card(j, i)
        self.next = 0

    #puts Deck back the way it was when it was first initialized    
    def reset(self):
        for i in range(0,4):
            for j in range(0,13):
                self.cards[j+i*13] = Card.Card(j, i)
        self.next = 0
     
    #returns the next card to be dealt
    def deal(self):
        self.next += 1
        return(self.cards[self.next - 1])

    #returns the number of cards in the deck that have not
    #been dealt since the last reset/shuffle.
    def cards_remaining(self):
        return 52 - self.next

    """cut the deck into two segments: the first n cards,
    called the "left", and the rest called the "right".  Note that
    either right or left might be empty.  Then, rearrange the deck
    to be the first card of the right, then the first card of the
    left, the 2nd of right, the 2nd of left, and so on.  Once one
    side is exhausted, fill in the remainder of the deck with the
    cards remaining in the othe rside.  Finally, make the first
    card in this shuffled deck the next card to deal.  For example,
    shuffle(26) on a newly-reset() deck results in: 2-clubs,
    2-spades, 3-clubs, 3-spades ... A-diamonds, A-hearts.
    
    Note: if shuffle is called on a deck that has already had some
    cards dealt, those cards should first be restored to the deck
    in the order in which they were dealt, preserving the most
    recent post-shuffled/post-reset state.
    This function produces no output."""
    def shuffle(self, n):
        
        left = [None]*n
        for i in range(0, n):
            left[i] = self.cards[i]
            print left[i]

        print ""
        right = [None]*(52-n)
        for i in range(n, 52):
            right[i-n] = self.cards[i]
            print right[i-n]

        print ""
        right_it = 0; left_it = 0
        if (n != 52):
            left_bool = False
        else:
            left_bool = True

        for i in range(0, 52):
#            print "i is %d" %i
            if not left_bool: 
                self.cards[i] = right[right_it]
                right_it += 1   
                if i <= 2*n :
                    left_bool = True
#                print "right card, which is", self.cards[i]
            elif left_bool:
                self.cards[i] = left[left_it]
                left_it += 1    
                if i < 2*(52-n) - 1:
                    left_bool = False
#                print "left card, which is", self.cards[i]
        self.next = 0

if __name__ == "__main__":
    #dostuff
    deck = Deck()
    import random
    # for i in range(0,52):
    #     print deck.cards[i]
    #     deck.cards[i] = Card.Card(random.randrange(0,13), random.randrange(0,4))
    # print ""

    # for i in deck.cards:
    #     print i
    # deck.reset()

    # print ""
    # for i in deck.cards:
    #     print i 
    # print ""
    # for i in range(0,52):
    #     print deck.deal()
    # deck.reset()


    deck.shuffle(26)
    for i in deck.cards:
        print i

    #deck.shuffle(28)
    #for i in deck.cards:
    #    print i

    #deck.shuffle(52)
    #for i in deck.cards:
    #    print i
